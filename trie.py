class TrieNode(object):
    def __init__(self, char):
        self.char = char
        self.children = {}
        self.word_finished = False
        self.word_counter = 0
        self.word_index_in_file = []


class Trie(object):
    def __init__(self, path: str, links: list, words: list):
        self.path_file = path
        self.root = TrieNode("*")
        self.links = links
        self.words = words

    def add(self, word: str, index_word_in_file: int):
        node = self.root
        for char in word:
            if char not in node.children.keys():
                node.children[char] = TrieNode(char)
            node = node.children[char]
        node.word_counter += 1
        if len(node.word_index_in_file) < 5:
            node.word_index_in_file.append(index_word_in_file)
        node.word_finished = True

    def search(self, word):
        current = self.root
        for char in word:
            if char not in current.children.keys():
                return False, current
            else:
                current = current.children[char]
        if current.word_finished:
            return current
        return False, current

    @staticmethod
    def get_child(current_node, char):
        for child in current_node:
            if child.char == char:
                return child
        return None

    @staticmethod
    def char_in_children(children, char):
        for child in children:
            if child.char == char:
                return True
        return False

    def find_word_from_prefix(self, node, word, words_for_autocomplete: list):
        if node.word_finished:
            words_for_autocomplete.append(word)
        for char, node in node.children.items():
            self.find_word_from_prefix(node, word + char, words_for_autocomplete)
        return words_for_autocomplete

    def autocomplete(self, key):
        node = self.root
        not_found = False
        temp_word = ""
        for a in list(key):
            if not node.children.get(a):
                not_found = True
                break
            temp_word += a
            node = node.children[a]
        if not_found:
            return []
        elif node.word_finished and not node.children:
            return []
        return self.find_word_from_prefix(node, temp_word, [])
