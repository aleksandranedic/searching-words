from termcolor import colored

from binary_save_and_load_graph import load_graph
from manage import find_words_in_files, find_words_from_prefix, did_you_mean

if __name__ == '__main__':
    print("Ucitavanje...")
    graph = load_graph()
    while True:
        sentence = input("\n\nUnesite reci koje zelite da trazite: (x za izlaz): ")
        if sentence.lower() == "x":
            break
        if '*' in sentence:
            words = find_words_from_prefix(graph, sentence[:-1])
            print("Izaberite jednu od sledecih reci:")
            for index, word in enumerate(words):
                if index != 0 and index % 5 == 0:
                    break
                else:
                    print(word, end=", ")
        else:
            results = find_words_in_files(graph, sentence)
            if len(results) == 0 and len(sentence.split(" ")) == 1:
                did_you_mean_word = did_you_mean(graph, sentence)
                print("Nema rezultata za trazenu rec, da li ste mislili na: ", did_you_mean_word)
            else:
                for index, result in enumerate(results):
                    if index != 0 and index % 10 == 0:
                        response = input("Da li zelite da nastavite? (x za izlazak, ostalo za nastavak): ")
                        if response == "x":
                            break
                    print(index + 1, ". ", result[0], ":\n",
                          " ".join(result[1][0:result[2]]) + " " +
                          colored(" ".join(result[1][result[2]:result[2] + result[3]]), "red")
                          + " " + " ".join(result[1][result[2] + result[3]:]), "\n")
