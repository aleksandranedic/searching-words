import pickle

from manage import make_graph


def save_graph():
    print("Graf se kreira...")
    graph = make_graph()
    with open('graph_binary_file.pkl', 'wb') as file:
        print('Graf se cuva u binarni fajl...')
        pickle.dump(graph, file)


def load_graph():
    with open('graph_binary_file.pkl', 'rb') as file:
        graph = pickle.load(file)
        return graph


if __name__ == '__main__':
    save_graph()
