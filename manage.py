from graph import Graph
from parser_project import Parser
import os
from trie import Trie
from copy import deepcopy
import shlex

ROOT = os.path.abspath("python-3.8.3-docs-html")


def get_path_files():
    list_of_files = []
    for root, dirs, files in os.walk(ROOT):
        for file in files:
            if root.__contains__(ROOT) and file.__contains__("html"):
                list_of_files.append(os.path.join(root, file))
    return list_of_files


def make_trie(words, path, links):
    trie = Trie(path, links, words)
    for index, word in enumerate(words):
        trie.add(word.lower(), index)
    return trie


def make_graph():
    graph = Graph(True)
    parser = Parser()
    files = get_path_files()
    nodes = {}
    for file_path in files:
        links, words = parser.parse(file_path)
        trie = make_trie(words, file_path, links)
        nodes[file_path] = graph.insert_vertex(trie)
    for node in nodes.values():
        for link in node.element().links:
            graph.insert_edge(node, nodes[link])
    return graph


def find_words_in_files(graph: Graph, sentence: str):
    words = shlex.split(sentence.lower())
    results = {}
    operations = []
    operands = []
    final_result = {}
    for i in range(len(words)):
        if i == 0:
            if " " not in words[i]:
                results = search_words_in_trie(graph, results, words[i])
            else:
                results = search_phrase_in_trie(graph, results, words[i])
            final_result = results
            operands.append(deepcopy(results))
        elif i % 2 == 1:
            operations.append(words[i])
        else:
            if " " not in words[i]:
                results = search_words_in_trie(graph, results, words[i])
            else:
                results = search_phrase_in_trie(graph, results, words[i])
            final_result = filter_results(operands[0], operations[0], results)
            operations.clear()
            operands.clear()
            operands.append(deepcopy(final_result))
    return sort(graph, final_result)


def search_words_in_trie(graph, results, word):
    for vertex in graph.vertices():
        trie_node = vertex.element().search(word)
        try:
            results[vertex.element().path_file] = [trie_node.word_counter, trie_node.word_index_in_file, [word]]
        except:
            results[vertex.element().path_file] = [0, [-1], []]
    return results


def search_phrase_in_trie(graph, results, phrase):
    words = phrase.split(' ')
    results_for_phrase = {}
    result_for_first_word = search_words_in_trie(graph, results, words[0])
    for path_file, counter_index in result_for_first_word.items():
        if counter_index[0] != 0:
            trie = find_trie_from_graph(graph, path_file)
            for one_appearance in counter_index[1]:
                for i in range(len(words)):
                    if trie.words[one_appearance + i].lower() != words[i]:
                        break
                    elif i == len(words) - 1:
                        if path_file in results_for_phrase.keys():
                            results_for_phrase[path_file][0] += 1
                            results_for_phrase[path_file][1].append(one_appearance)
                        else:
                            results_for_phrase[path_file] = [1, [one_appearance], [phrase]]

    return results_for_phrase


def find_trie_from_graph(graph, path_file):
    for vertex in graph.vertices():
        if vertex.element().path_file == path_file:
            return vertex.element()
    return None


def filter_results(first_dict: dict, operation: str, second_dict: dict):
    filtered_dict = {}
    if operation == "and":
        for file in first_dict.keys():
            try:
                if first_dict[file][0] != 0 and second_dict[file][0] != 0:
                    filtered_dict[file] = [first_dict[file][0] + second_dict[file][0],
                                           first_dict[file][1] + second_dict[file][1],
                                           first_dict[file][2] + second_dict[file][2]]
            except KeyError:
                continue
    elif operation == "not":
        for file in first_dict.keys():
            try:
                if first_dict[file][0] != 0 and second_dict[file][0] == 0:
                    filtered_dict[file] = [first_dict[file][0] + second_dict[file][0],
                                           first_dict[file][1] + second_dict[file][1],
                                           first_dict[file][2] + second_dict[file][2]]
            except KeyError:
                filtered_dict[file] = [first_dict[file][0],
                                       first_dict[file][1],
                                       first_dict[file][2]]
    elif operation == "or":
        for file in first_dict.keys():
            try:
                if first_dict[file][0] != 0 or second_dict[file][0] != 0:
                    filtered_dict[file] = [first_dict[file][0] + second_dict[file][0],
                                           first_dict[file][1] + second_dict[file][1],
                                           first_dict[file][2] + second_dict[file][2]]
            except KeyError:
                if first_dict[file][0] != 0:
                    filtered_dict[file] = [first_dict[file][0],
                                           first_dict[file][1],
                                           first_dict[file][2]]
                continue
    return filtered_dict


def sort(graph: Graph, results: dict):
    rang_dict = {}
    for vertex in graph.vertices():
        try:
            number_of_words = find_number_of_words(results[vertex.element().path_file][2])
            rang_dict[vertex.element().path_file] = (
                rang(results, find_links_to_file(vertex, graph), results[vertex.element().path_file][0]),
                find_paragraph(vertex.element().words, results[vertex.element().path_file][1], number_of_words),
                results[vertex.element().path_file][2])
        except KeyError:
            continue
    sorted_list = sorted(rang_dict.items(), key=lambda x: x[1], reverse=True)
    return reformat_for_print(sorted_list)


def find_links_to_file(vertex, graph: Graph):
    links_to_file = []
    edges = graph.incident_edges(vertex, False)
    for edge in edges:
        links_to_file.append(edge.opposite(vertex).element().path_file)
    return links_to_file


def find_number_of_words(words: list):
    if len(words) != 0:
        try:
            word = words[0].split(" ")
            return len(word)
        except:
            return 1
    return 0


def rang(results: dict, links: list, number_of_words_in_file: int):
    num_of_words_in_links = 0
    for link in links:
        try:
            num_of_words_in_links += results[link][0]
        except:
            continue
    return 25 * number_of_words_in_file + 30 * len(links) + 35 * num_of_words_in_links


def find_paragraph(words: list, index_of_appearance: list, num_of_words: int):
    paragraph_list = []
    for one_appearance in index_of_appearance:
        if one_appearance == -1:
            continue
        if one_appearance < 5:
            beginning = 0
            index_of_word_in_list = one_appearance
        else:
            beginning = one_appearance - 5
            index_of_word_in_list = 5
        list_of_words = words[beginning:one_appearance + num_of_words + 5]
        paragraph_list.append([list_of_words, index_of_word_in_list, num_of_words])
    return paragraph_list


def reformat_for_print(results: list):
    reformatted_list = []
    for result in results:
        for paragraph in result[1][1]:
            if paragraph:
                reformatted_list.append((result[0], paragraph[0], paragraph[1], paragraph[2]))
    return reformatted_list


def find_words_from_prefix(graph: Graph, prefix: str):
    all_words = []
    for vertex in graph.vertices():
        for word in vertex.element().autocomplete(prefix):
            if word not in all_words:
                all_words.append(word)
    return all_words


def did_you_mean(graph: Graph, word: str):
    correct_word = ""
    different_letters = 1000
    for vertex in graph.vertices():
        for trie_word in vertex.element().words:
            num_of_letters = _num_of_different_letters(word, trie_word)
            if num_of_letters < different_letters:
                correct_word = trie_word
                different_letters = num_of_letters
                if num_of_letters < 3:
                    return correct_word
    return correct_word


def _num_of_different_letters(word: str, trie_word: str):
    difference = 0
    for index in range(min(len(word), len(trie_word))):
        if word[index] != trie_word[index]:
            difference += 1
    difference += abs(len(trie_word) - len(word))
    return difference
