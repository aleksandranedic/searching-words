from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import pyqtSlot, QSize
from PyQt5.QtGui import QFont, QColor, QImage, QPalette, QBrush
from PyQt5.QtWidgets import QApplication, QMainWindow, QLineEdit, QPushButton, QMessageBox
import sys

from binary_save_and_load_graph import load_graph
from manage import find_words_from_prefix, find_words_in_files, did_you_mean


class Window(QMainWindow):
    def __init__(self, graph):
        super().__init__()
        self.text = None
        self.graph = graph
        self.first_displaying_index = 0
        self.title = 'Search engine'
        self.setWindowIcon(QtGui.QIcon('search.jpg'))
        self.left = 350
        self.top = 100
        self.width = 1250
        self.height = 850
        self.make_dark_mode()
        self.init_ui()

    @staticmethod
    def make_dark_mode():
        QApplication.setStyle("Fusion")
        palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.Window, QtGui.QColor(53, 53, 53))
        palette.setColor(QtGui.QPalette.WindowText, QtCore.Qt.white)
        palette.setColor(QtGui.QPalette.Base, QtGui.QColor(25, 25, 25))
        palette.setColor(QtGui.QPalette.AlternateBase, QtGui.QColor(53, 53, 53))
        palette.setColor(QtGui.QPalette.ToolTipText, QtCore.Qt.white)
        palette.setColor(QtGui.QPalette.Text, QtCore.Qt.white)
        palette.setColor(QtGui.QPalette.Button, QtGui.QColor(53, 53, 53))
        palette.setColor(QtGui.QPalette.BrightText, QtCore.Qt.red)

        oImage = QImage("backgroung.jpg")
        sImage = oImage.scaled(QSize(1250, 850))
        palette.setBrush(QPalette.Window, QBrush(sImage))

        palette.setColor(QtGui.QPalette.Highlight, QtGui.QColor(142, 45, 197).lighter())
        palette.setColor(QtGui.QPalette.HighlightedText, QtCore.Qt.black)
        app.setPalette(palette)

    def init_ui(self):
        self.setWindowTitle(self.title)
        self.setStyleSheet(" background - image: url(backgroung.jpg);")
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.main_label = QtWidgets.QLabel(self)
        self.main_label.resize(1050, 40)
        self.main_label.setFont(QFont("CASTELLAR", 30, False, True))
        self.main_label.setText("SEARCH ENGINE")
        self.main_label.setStyleSheet("color: white;")
        self.main_label.move(400, 30)

        self.textbox = QLineEdit(self)
        self.textbox.setFont(QFont('Montserrat', 15))
        self.textbox.setStyleSheet("color: rgb(169,169,169);")
        self.textbox.move(20, 100)
        self.textbox.resize(1050, 40)

        self.display_text = QtWidgets.QTextBrowser(self)
        self.display_text.setFont(QFont('Montserrat', 13))
        self.display_text.setReadOnly(True)
        self.display_text.move(20, 150)
        self.display_text.resize(1183, 610)

        self.search_button = QPushButton('Search', self)
        self.search_button.setFont(QFont('Segoe UI Light', 14))
        self.search_button.setStyleSheet("color: rgb(220,220,220);")
        self.search_button.move(1100, 100)
        self.search_button.resize(100, 40)
        self.search_button.clicked.connect(self.on_click)

        self.next_button = QPushButton('Next page', self)
        self.next_button.setFont(QFont('Segoe UI Light', 12))
        self.next_button.setStyleSheet("color: rgb(220,220,220);")
        self.next_button.move(650, 780)
        self.next_button.resize(185, 50)
        self.next_button.clicked.connect(self.next_text)

        self.previous_button = QPushButton('Previous page', self)
        self.previous_button.setFont(QFont('Segoe UI Light', 12))
        self.previous_button.setStyleSheet("color: rgb(220,220,220);")
        self.previous_button.move(385, 780)
        self.previous_button.resize(185, 50)
        self.previous_button.clicked.connect(self.previous_text)

        self.show()

    def show_text(self):
        self.display_text.setText("")
        if self.text:
            text = ""
            for i in range(self.first_displaying_index, self.first_displaying_index + 10):
                try:
                    first_row = str(i + 1) + ". " + self.text[i][0] + ":\n"
                    first_part = " ".join(self.text[i][1][0:self.text[i][2]]) + " "
                    colored_text = " ".join(self.text[i][1][self.text[i][2]:self.text[i][2] + self.text[i][3]])
                    last_part = " " + " ".join(self.text[i][1][self.text[i][2] + self.text[i][3]:]) + "\n\n"
                    text += first_row + first_part + colored_text + last_part

                    white_color = QColor(169,169,169)
                    purple_color = QColor(218, 112, 214)
                    self.display_text.setTextColor(white_color)
                    white_text = first_row + first_part
                    self.display_text.append(white_text)

                    self.display_text.setTextColor(purple_color)
                    purple_text = colored_text
                    self.display_text.insertPlainText(purple_text)

                    self.display_text.setTextColor(white_color)
                    white_text = last_part
                    self.display_text.insertPlainText(white_text)
                except IndexError:
                    continue

    @pyqtSlot()
    def previous_text(self):
        self.first_displaying_index -= 10
        if self.first_displaying_index >= 0:
            self.show_text()
        else:
            self.first_displaying_index = 10

    @pyqtSlot()
    def next_text(self):
        self.show_text()
        self.first_displaying_index += 10

    @pyqtSlot()
    def on_click(self):
        self.first_displaying_index = 0
        sentence = self.textbox.text()
        if '*' in sentence:
            words = find_words_from_prefix(self.graph, sentence[:-1])
            if len(words) == 0:
                QMessageBox.question(self, 'Error', "Ne postoji rec sa ovim pocetnim slovima. ", QMessageBox.Ok,
                                     QMessageBox.Ok)
                return
            elif len(words) > 5:
                words = words[:6]
            QMessageBox.question(self, 'Autocomplete', "Izaberite jednu od sledecih reci: " +
                                 ", ".join(words), QMessageBox.Ok, QMessageBox.Ok)
        else:
            results = find_words_in_files(self.graph, sentence)
            self.text = results
            if len(results) == 0 and len(sentence.split(" ")) == 1:
                did_you_mean_word = did_you_mean(self.graph, sentence)
                QMessageBox.question(self, 'Did you mean',
                                     "Nema rezultata za trazenu rec, da li ste mislili na: " + did_you_mean_word,
                                     QMessageBox.Ok, QMessageBox.Ok)
            elif len(results) == 0:
                QMessageBox.question(self, 'Error', "Nema rezultata za trazenu pretragu.",
                                     QMessageBox.Ok, QMessageBox.Ok)
            else:
                self.show_text()
                self.first_displaying_index += 10


if __name__ == '__main__':
    graph = load_graph()
    app = QApplication(sys.argv)
    ex = Window(graph)
    sys.exit(app.exec_())
